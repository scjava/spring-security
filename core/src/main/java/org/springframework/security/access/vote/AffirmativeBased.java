/*
 * Copyright 2004, 2005, 2006 Acegi Technology Pty Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.security.access.vote;

import java.util.Collection;
import java.util.List;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;

/**
 * Simple concrete implementation of
 * {@link org.springframework.security.access.AccessDecisionManager} that grants access if
 * any <code>AccessDecisionVoter</code> returns an affirmative response.
 */
public class AffirmativeBased extends AbstractAccessDecisionManager {

	public AffirmativeBased(List<AccessDecisionVoter<?>> decisionVoters) {
		super(decisionVoters);
	}

	/**
	 * This concrete implementation simply polls all configured
	 * {@link AccessDecisionVoter}s and grants access if any
	 * <code>AccessDecisionVoter</code> voted affirmatively. Denies access only if there
	 * was a deny vote AND no affirmative votes.
	 * <p>
	 * If every <code>AccessDecisionVoter</code> abstained from voting, the decision will
	 * be based on the {@link #isAllowIfAllAbstainDecisions()} property (defaults to
	 * false).
	 * </p>
	 * @param authentication the caller invoking the method
	 * @param object the secured object
	 * @param configAttributes the configuration attributes associated with the method
	 * being invoked
	 * @throws AccessDeniedException if access is denied
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
			throws AccessDeniedException {
		int deny = 0;
		/**
		 * 这种投票方式就是将请求路径url上的所有权限属性configAttributes一次性传入投票器
		 * 投票器，如果是java web的方式配置的，就是http.antMatcher...的投票器就是WebExpressionVoter
		 * 比如注解的方式那么就有很多种
		 * Jsr250Voter
		 * PreInvocationAuthorizationAdviceVoter
		 * 等等，反正有很多中，根据你配置了那些，比如注解@PreAuthorize()就会是PreInvocationAuthorizationAdviceVoter来进行投票
		 * getDecisionVoters() 这个就是获取投票管理器列表，这个应该是在启动扫描的时候根据url就已经生成了对应的投票管理器了
		 *
		 */

		for (AccessDecisionVoter voter : getDecisionVoters()) {
			int result = voter.vote(authentication, object, configAttributes);
			/**
			 * 投票结果，只有三种，同意、拒绝、弃权
			 * 这个是如果只要有一个同意，就直接返回授权成功
			 */
			switch (result) {
				//投票同意，返回成功
			case AccessDecisionVoter.ACCESS_GRANTED:
				return;
			case AccessDecisionVoter.ACCESS_DENIED:
				//投票失败，登记失败次数
				deny++;
				break;
			default:
				break;
			}
		}
		//只要拒绝次数大于0，直接抛出授权失败的异常
		if (deny > 0) {
			throw new AccessDeniedException(
					this.messages.getMessage("AbstractAccessDecisionManager.accessDenied", "Access is denied"));
		}
		// To get this far, every AccessDecisionVoter abstained
		//代码执行到这里的时候表示所有的投票结束了，但是也没有拒绝，也没有同意
		//所以这里检查是否要 抛出异常，通过 allowIfAllAbstainDecisions控制的，默认是false
		checkAllowIfAllAbstainDecisions();
	}

}
